let global

try {
  global = window
}
catch(e) {
  global = module.exports
  location = {}
}


const configs = new Map([
  ['default', {
    apiUrl: '/api'
  }],
  ['localhost:8080', {
    apiUrl: 'http://localhost:3579'
  }]
])

const translations = new Map([
  ['en', "English"],
  ['fr', "French"],
  ['it', "Italian"],
  ['es', "Spanish"],
  ['ru', "Russian"],
  ['de', "German"],
  ['pt', "Portuguese"],
])

const facetSettings = [
  { name: 'kind', icon: 'kind', title: 'Kind' },
  { name: 'contact', icon: 'contact', title: 'General', inList: false },
  { name: 'grouping', icon: 'grouping', title:  'Disease Grouping' },
  { name: 'country', icon: 'Country', title: 'Country' },
  { name: 'group_kind', icon: 'groupkind', title: 'Group > Kind', modifiers: true },
  { name: 'event_kind', icon: 'eventkind', title: 'Event > Kind' },
  { name: 'keyword_kind', icon: 'keywordkind', title: 'Keyword > Kind' },
  { name: 'group', icon: 'group', title: 'Group', modifiers: true },
  { name: 'event', icon: 'event', title: 'Event' },
  { name: 'keyword', icon: 'keyword', title: 'Keyword' },
  { name: 'group_role', icon: 'grouprole', title: 'Group > Role', modifiers: true },
  { name: 'event_role', icon: 'eventrole', title: 'Event > Role' },
  { name: 'disease', icon: 'disease', title: 'Disease' },
  { name: 'membership', icon: 'membership', title: 'Membership' },
]

global.config = configs.get(location.host) || configs.get('default')

async function init(callback) {
  // Load config before user
  const response = await api('/config')
  Object.assign(global.config, response.data)
  const token = localStorage.getItem('token')
  if(token) await global.user.authenticate(token)
  callback && callback()
}

function request (url, options = {}) {
  if (options.loader) showLoader()
  const params = Object.assign(options, { mode: 'cors' })
  let response = null
  if (options.data) options.body = JSON.stringify(options.data)
  if (options.body && !options.method) options.method = 'post'
  options.method = options.method && options.method.toUpperCase()
  return fetch(url, params)
    .then((r) => {
      if (options.loader) hideLoader()
      response = r
      response.isBinary = false
      const contentType = r.headers.get('Content-Type')
      if(!contentType || contentType.startsWith('text/')) return r.text()
      if(contentType.startsWith('application/json')) return r.json()
      r.isBinary = true
      if(contentType.startsWith('application/vnd.openxmlformats')) return r.arrayBuffer()
      return r.blob()
    })
    .then((data) => {
      response.data = data
      return response
    })
    .catch(console.error.bind(console))
}

function api (uri, options = {}) {
  const url = `${config.apiUrl}${uri}`
  options.headers = { 'api-key': localStorage.getItem('token') }
  if(localStorage.ghostMode) options.headers['ghost'] = 1
  return request(url, options)
}

function apiResponse (response) {
  if(!response.ok) {
    let message = response.data.error
    if(!message) message = `The API responded with an error ${response.status}`
    notify(message, 'error')
  }
  if(response.status == 401) {
    global.location.href = '/logout'
    throw new Error(response.error)
  }
  return response
}

function showLoader() {
  document.querySelector('app').classList.add('loading')
}

function hideLoader() {
  document.querySelector('app').classList.remove('loading')
}

function urlParams (queryString) {
  if (queryString === undefined) queryString = location.search.slice(1)
  const keyValues = queryString.startsWith('&') ? queryString.slice(1) : queryString
  return new Map(
    keyValues
      .split('&')
      .map(kv => kv.split('='))
      .map(kv => [kv[0], decodeURIComponent(kv[1])])
  )
}

function objectToUrlParams (obj) {
  return Object.keys(obj).map(name => {
    let value = obj[name]
    if (!value) return
    if (value.join) value = value.join('||')
    return value && `${name}=${encodeURIComponent(value)}`
  }).filter(o => o).join('&')
}

function mapToObject(map) {
  const obj = {}
  map.forEach((v, k) =>  k && (obj[k] = v))
  return obj
}

function enablePageQueryParams() {
  page('*', (ctx, next) => {
    ctx.query = queryParams()
    next()
  })
}

function replaceQueryParams(params, preserve = true) {
  const [pathname, queryString] = page.current.split('?')
  const currentParams = mapToObject(urlParams(queryString))
  const updatedParams = Object.assign(preserve ? currentParams : {}, params)
  return `${pathname}?${objectToUrlParams(updatedParams)}`
}

function queryParams() {
  return urlParams(page.current.split('?')[1] || '')
}

function getQueryParams() {
  return mapToObject(queryParams())
}

function getUrl(url, paramsOverride = { showFacets: false }) {
  const oldUrl = replaceQueryParams(paramsOverride)
  const [pathname, queryString] = oldUrl.split('?')
  return queryString ? `${url}?${queryString}` : url
}

function updatePage() {
  page(location.href.replace(location.origin, ''))
}

function notify(message, type) {
  if(typeof message !== 'string') message = JSON.stringify(message)
  document.querySelector('toast').show(message, type)
  console[type] ? console[type](message) : console.log(message)
}

function fill(text, length, fillWith) {
  return text.toString().padStart(length, fillWith)
}

function trans(text, options = {}) {
  if (text.constructor === String) return translations.get(text) || text
  if (text.constructor === Date) {
    const date = `${fill(text.getDate(), 2, '0')}/${fill(text.getMonth() + 1, 2, '0')}/${text.getFullYear()}`
    return options.time ? `${date} ${fill(text.getHours(), 2, '0')}:${fill(text.getMinutes(), 2, '0')}` : date
  }
  if (Object.keys(text).length) return JSON.stringify(text)
  if (text.map) return text.map(t => trans(t))
  return text
}


/**
* return the value of a list of [{key: '', value: ''}, …] passing the key.
* Useful for quickly reading from config
*/
function keyToValue(list, key) {
  const obj = list.find(l => l.key === key)
  return obj && obj.value
}


function ucFirst(str) {
  if(!str.slice) return str
  return str.slice(0, 1).toLocaleUpperCase() + str.slice(1)
}


function classifyByKind(list) {
  return list.reduce((acc, item) => {
    item.types = []
    if(item.for_person) item.types.push('contacts')
    if(item.for_organisation) item.types.push('organisations')
    if(!item.kind) item.kind = ' others'
    const category = acc.find(g => g.kind === item.kind)
    if(!category) acc.push({ kind: item.kind, values: [] })
    acc.find(g => g.kind === item.kind).values.push(item)
    return acc
  }, []).sort((a, b) => (a.kind && a.kind < b.kind) ? -1 : 1)
}


function classForFilter(name) {
  let className = name
  if(name.indexOf('_') > 0) className = name.split('_')[0]
  return className
}


function diffing(old_, new_, ignoreFields = []) {
  return Object.keys(new_)
    .filter(k => !ignoreFields.includes(k))
    .reduce((diffs, k) => {
      if((new_[k] || old_[k]) &&
         (JSON.stringify(new_[k]) != JSON.stringify(old_[k])))
          diffs[k] = new_[k]
      return diffs
  }, {})
}

function sort(arr) {
  return arr ? arr.sort((a, b) => a - b) : []
}

function serializeForm(form, keepEmpty = false, data = {}) {
  const subForms = Array.from(form.querySelectorAll('[subform]'))
  const subFields = form.querySelector('[subform]') && Array.from(form.querySelectorAll('[subform] [name]')) || []
  const rootFields = Array.from(form.querySelectorAll('[name]')).filter(n => !subFields.includes(n))
  rootFields.forEach((f) => {
    let name = f.getAttribute('name')
    if(name.startsWith('_')) return
    let value = null
    if(f.type === 'checkbox') {
      if(f.value === 'true') value = f.checked
      else value = f.checked ? f.value : ''
    }
    else {
      value = f.value
      if (value && value.trim) value = f.value.trim()
      if (!value) {
        value = f.getAttribute('value')
        if (value && value.trim) value = f.value.trim()
      }
    }
    if(String(Number(value)) === value) value = Number(value)
    try {
      const jsoned = JSON.parse(value)
      if(Array.isArray(jsoned)) value = jsoned
    } catch {}
    if (value !== false && !value && !keepEmpty) return
    if(name.endsWith('[]')) {
      name = name.slice(0, -2)
      if (!data.hasOwnProperty(name)) data[name] = []
      data[name].push(value)
    }
    else data[name] = value
  })
  if(subForms) {
    subForms.forEach(f => {
      let name = f.getAttribute('subform')
      if(name && name.endsWith('[]')) {
        name = name.slice(0, -2)
        if (!data.hasOwnProperty(name)) data[name] = []
        data[name].push(serializeForm(f, keepEmpty))
      }
      else data[name] = serializeForm(f, keepEmpty)
    })
  }
  return data
}

async function downloadUrl(url, filename) {
  notify('Your file is being prepared. Download starting soon…')
  const response = apiResponse(await api(url))
  if (response.ok) download(filename, response.data, response.headers)
  return response
}

function download(filename, content, headers, body = document.body) {
  const type = headers['Content-Type'] || (headers.get && headers.get('Content-Type')) || headers
  const blob = new File([content], filename, { type })
  const el = document.createElement('a')
  el.href = URL.createObjectURL(blob)
  el.download = filename
  body.appendChild(el)
  el.click()
  body.removeChild(el)
}

function downloadJsonReportToCsv(reports, filename, contextName) {
  const uselessKeys = ['uri', 'label']
  const valuesSeparator = ';'

  const {fields, values} = reports.reduce((acc, report) => {
    const value = {}

    Object.keys(report.raw).map(rawKey => {
      value[`xl/${rawKey}`] = report.raw[rawKey]
      if (!acc.fields.includes(`xl/${rawKey}`)) acc.fields.push(`xl/${rawKey}`)
    })
    Object.keys(report.data).map(dataKey => {
      if (uselessKeys.includes(dataKey)) return
      value[`db/${dataKey}`] = report.data[dataKey]
      if (!acc.fields.includes(`db/${dataKey}`)) acc.fields.push(`db/${dataKey}`)
    })

    value['status'] = report.status
    if (!acc.fields.includes('status')) acc.fields.push('status')

    Object.keys(report.report).map(reportKey => {
      value[`info/${reportKey}`] = report.report[reportKey]
      if (!acc.fields.includes(`info/${reportKey}`)) acc.fields.push(`info/${reportKey}`)
    })

    acc.values.push(value)

    return acc
  }, {fields: [], values: []})

  const notEmptyFields = fields.filter(field =>
    !values.reduce((empty, value) => {
      if (!empty) return empty
      if (value[field]) return false
      return true
    }, true)
  )

  // Head line
  const csvLines = [`${notEmptyFields.join(valuesSeparator)}`]

  values.forEach(value => {
    csvLines.push(notEmptyFields.map(field  => value[field] ? `"${value[field]}"` : '').join(valuesSeparator))
  })

  const csv = csvLines.join('\n')

  const d = new Date()
  const date = `${d.getFullYear()}${d.getMonth()+1}${d.getDate()}-${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`

  const dumpFilename = `dump-${date}-${contextName}-${filename}.csv`

  download(dumpFilename, csv, 'text/csv')
}

async function loadAdvancedConfig() {
  let response = await api(`/keyword`)
  Object.assign(global.config, { KEYWORDS: response.data.data })
  response = await api(`/group`)
  Object.assign(global.config, { GROUPS: response.data.data })
}

async function searchResults(filters = []) {
  const contactFilter = filters.find(f => f.name === 'contact')
  if (contactFilter) {
    filters = filters.filter(f => f.name !== 'q')
    filters.push({ name: 'q', value: contactFilter.value })
  }
  const searchParams = filters.map(f => `${f.name}=${encodeURIComponent(f.value)}`).join('&')
  const response = apiResponse(await api(`/search?${searchParams}`))
  return response && response.data
}

/**
 * Extract the actual value and the modifier out of a raw pk or value.
 *
 * @param {String} str the value and modifier to read from.
 * @returns {Object} an { modifier, value } object where modifier can
 * be a 1 or 2 characters string (on of: <>*+- ) and value is the actual
 * value.
 */
function getvalue(value) {
  value = String(value || '')
  const modifiers = '+-'.split('')
  const statuses = '<>*'.split('')
  let modifier = ''
  let status = ''
  if (statuses.includes(value.slice(0, 1))) {
    status = value.slice(0, 1)
    value = value.slice(1)
  }
  if (modifiers.includes(value.slice(0, 1))) {
    modifier = value.slice(0, 1)
    value = value.slice(1)
  }
  return { value, status, modifier }
}

/**
 * Creates a filter value based on a raw value, a modifier and a status.
 * Ex: createvalue(5, '+', '*') -> '*+5'
 * modifier can be "", "+", "-"
 * status can be "*", "<", ">"
 */
function createvalue(raw, modifier, status) {
  const structured = getvalue(raw)
  if(typeof modifier !== 'string') modifier = structured.modifier
  if(typeof status !== 'string') status = structured.status
  const value = structured.value
  return `${status}${modifier}${value}`
}

class Role {
  constructor({ person, organisation, groups, events, name, email, phone, is_primary, is_default, is_voting, is_archived }) {
    this.person = person && person.pk
      ? Number(person.pk)
      : Number(person)
    this.organisation = organisation && organisation.pk
      ? Number(organisation.pk)
      : Number(organisation)
    this.name = name
    this.email = email
    this.phone = phone
    this.groups = (groups || []).map(g => new Group(g))
    this.events = (events || []).map(e => new Event(e))
    this.is_primary = Boolean(is_primary)
    this.is_default = Boolean(is_default)
    this.is_voting = Boolean(is_voting)
    this.is_archived = Boolean(is_archived)
  }
}

class Event {
  constructor({ event, role }) {
    this.event = event && event.pk
      ? Number(event.pk)
      : Number(event)
    if(role) this.role = role
  }
}

class Membership {
  constructor({ organisation, status, start_date, end_date, fees }) {
    if(organisation) this.organisation = Number(organisation)
    if(status) this.status = status
    if(start_date) this.start_date = start_date
    if(end_date) this.end_date = end_date
    if(fees) this.fees = fees
  }
}

class Group {
  constructor({ group, role, is_active }) {
    if(group) this.group = group.pk ? Number(group.pk) : Number(group)
    if(role) this.role = role
    this.is_active = Boolean(is_active)
  }
}

class User {
  constructor() {
    this.props = {}
  }

  async authenticate(token) {
    const response = await api('/me')
    if(response.ok) {
      this.props = response.data
      await loadAdvancedConfig()
    }
  }

  logout() {
    this.props = {}
  }

  get(prop) {
    return Object.keys(this.props).includes(prop) && this.props[prop]
  }

  can(action) {
    const scopes = this.props.scopes
    return scopes && scopes.includes(action)
  }

  isAuthenticated() {
    return !!this.props.pk
  }
}

global.getvalue = getvalue
global.createvalue = createvalue
global.facetSettings = facetSettings

global.user = new User()
